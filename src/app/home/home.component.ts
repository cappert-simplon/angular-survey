import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  surveyForm = new FormGroup({
    questions: new FormGroup({
      response1: new FormControl(),
      response2: new FormControl()
    }),
    coordonnees: new FormGroup({
      nom: new FormControl(),
      email: new FormControl()
    })
  })

currentState: string = "questions";

  constructor() { }

  ngOnInit(): void {
  }

  get response1():String { return this.surveyForm.get('questions')?.get('response1')?.value }
  get response2() { return this.surveyForm.get('questions')?.get('response2')?.value  }
  get nom() { return this.surveyForm.get('coordonnees')?.get('nom')?.value }
  get email() { return this.surveyForm.get('coordonnees')?.get('email')?.value }

  get themes() {
          // process list from csv
      // split string
      let themeAry = this.response1.split(',');
      
            // remove spaces (trim)
      let themes =  themeAry.map( (each) => { each.trim()});
      // themeAry.forEach(element => { element.trim() });

      return themeAry;
  }

  onSubmit()  {
    if (this.currentState == 'questions') {
      this.currentState = 'coordonnees'
    }

    // récapitulatif (if is recap etc...)

    console.log('coordonnées ', this.surveyForm.get('coordonnees')?.value.email);
  }

  public goBack() {
    if (this.currentState == 'coordonnees') {
      this.currentState = 'questions'
    }

  }

}
