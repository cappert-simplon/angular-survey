## Exercice : 
  1. Créer une projet angular "angular-survey" avec le routage et le CSS
  2. Créer un composant Home, et affectez-le à la route par défaut
  3. Vérifiez que tout fonctionne

On va faire un sondage pour choisir le thème des futurs sondages

Créer dans le template du HomeComponent un formulaire avec la question suivante :

> Quels sont vos 3 thèmes favoris, séparés par des virgules ?"

Ajoutez un champ de réponse libre (input text)


Un petit tuto pour comprendre les ReactiveForms : https://guide-angular.wishtack.io/angular/formulaires/reactive-forms/la-boite-a-outils-des-reactive-forms

A small tuto on ReactiveForms : https://www.digitalocean.com/community/tutorials/angular-reactive-forms-introduction

## Suite de l'exercice : 
  6. Ajoutez une deuxième question : "Aimez-vous répondre à des sondages ?"
  7. Ajoutez un champ de réponse libre (input text)
  8. Ajoutez une question "Comment vous appelez-vous ?" et un champ de réponse libre
  9. Ajoutez une question "Quelle est votre adresse email ?" et un champ de réponse libre
  10. Transformer les 4 variables en 1 FormGroup
  11. Affecter le FormGroup au formulaire

Exercice : En se basant sur l'attribut currentState du composant, faire apparaître la bonne étape du formulaire
Suite : rajouter une étape à la fin du formulaire, pour permettre à l'utilisateur de vérifier tout ce qu'il a saisi avant de terminer
Bonus : permettre à l'utilisateur de revenir en arrière dans le formulaire, pour modifier sa saisie

### Exemple de page récap : 

Merci d'avoir rempli notre sondage.

Avant de terminer, merci de vérifier les informations que vous nous avez données :


Thèmes : 
    - titi
    - OL
    - politique

Votre préférence sur les sondages : "Oui, j'adore !!!!!! <3"

Vous vous appelez Barth et on peut vous écrire sur votre adresse barth@barthdeluy.me

[Terminer le questionnaire]

Suite de l'exercice : 
  Dans la "reponse2" côté template HTML, remplacer le "input type=text" par 2 boutons radio "Oui"/"Non"
￼
Suite de l'exercice : rajouter une 3ème question au sondage : "Quelles couleurs aimez-vous ?", et afficher 4 cases à cocher "Bleu", "Rouge, "Vert", "Jaune"

Aide : https://edupala.com/how-to-implement-angular-checkbox-input/
